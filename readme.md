# ppp.css
## PrePostPrint only css framework

### Installation
- Ajouter le fichier ppp.css dans le dossier de votre site
- Lier le fichier ppp.css à votre html ``<link rel="stylesheet" href="ppp.css">``

### Usage

#### Navigateur

Pour visualiser et imprimer votre publication utiliser Chrome ou Chromium comme navigateur.

#### Éditer les variables de format de la publication
Au début du fichier ppp.css, éditer les variables.
<pre><code>:root{
  --page-width: 14.85; //largeur de la page
  --page-height: 21cm; //hauteur de la page
  --page-margin: 0.8cm; //marge de la page
  //position du numéro de page
  --page-footer-bottom: 0.4cm; 
  --page-footer-margin: 0.8cm;
}
</code>
</pre>


#### Structure HTML
Chaque page se structure ainsi <br>
``<section class="page"><!--  html content --></section>``

Une couverture ce struture ainsi 

```html
<div class="page">
	<section class=" cover">
		<!-- quatrieme de couverture -->
		<div id="fourth" class="couv">
			<!--  html content -->
		</div>
		<!-- dos -->
		<div id="back" class="couv">
			<!--  html content -->
		</div>
		<!-- premiere de couverture -->
		<div id="first" class="couv">
			<!--  html content -->
		</div>
	</section>
</div>
```
#### Ajouter un header
Ajouter une classe sur une .page <br>
exemple: ``<section class="page introheader"></section>`` <br>
Utiliser ``::before`` en css, et remplacer content: par le texte de son header<br>
exemple:  ``.introheader::before{ content:"header";}``

#### Utiliser le framework
- ``.print``: les éléments avec cette classe s'affiche seulement à l'impression
- ``.screen``: les éléments avec cette classe s'affiche seulement à l'écran
- ``.center``: centre l'élément horizontalement
- ``.center-top``: centre l'élément horizontalement
- ``.nopagination``: enlève la pagination des pages (.page) ayant cette classe
- ``.plainimage``: ajouter cette classe à un élément img pour adapter l'image à la page



